<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectDb.class.php";

class ProductDbDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    public function __construct() {
        $this->connect = (new ConnectDb())->getConnection();
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): ProductDbDAO {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($product): bool {
      if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {
            $sql = <<<SQL
                INSERT INTO product (id,name,description,category,price) VALUES (:id,:name,:description,:category,:price);
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":id", $product->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $product->getName(), PDO::PARAM_STR);
            $stmt->bindValue(":description", $product->getDescription(), PDO::PARAM_STR);
            $stmt->bindValue(":category", $product->getCategory(), PDO::PARAM_STR);
            $stmt->bindValue(":price", $product->getPrice(), PDO::PARAM_STR);

            $stmt->execute(); // devuelve TRUE o FALSE

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function modify($product): bool {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {
            
          $sql=<<<SQL
                    UPDATE product SET id=:id,name=:name,description=:description,category=:category,price=:price WHERE ID=:id;
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":id", $product->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $product->getName(), PDO::PARAM_STR);
            $stmt->bindValue(":description", $product->getDescription(), PDO::PARAM_STR);
            $stmt->bindValue(":category", $product->getCategory(), PDO::PARAM_STR);
            $stmt->bindValue(":price", $product->getPrice(), PDO::PARAM_STR);

            $stmt->execute(); 

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function delete($id): bool {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {
            
             $sql=<<<SQL
                    DELETE FROM product WHERE id=:id;
SQL;
            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
           
            $stmt->execute(); 

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function listAll(): array {
         $result = array();

        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return $result;
        };

        try {
            $sql = <<<SQL
                SELECT id,name,description,category,price FROM product;
SQL;

            $result = $this->connect->query($sql); // devuelve los datos

            $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Product');

            return $result->fetchAll();
        } catch (PDOException $e) {
            return $result;
        }

        return $result;
    }

    public function searchById($id) {
          if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return NULL;
        };

        try {
            $sql = <<<SQL
                SELECT id,name,description,category,price FROM product WHERE id=:id;
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);

            $stmt->execute(); 

            if ($stmt->rowCount()) {
                $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Product');
                return $stmt->fetch();
            } else {
                return NULL;
            }
        } catch (PDOException $e) {
            return NULL;
        }
    }
    
    public function categoryInProduct($idcategory){
         if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return NULL; //ERROR
        };

        try {
            $sql = <<<SQL
                SELECT id,name,description,category,price FROM product WHERE category=:idcategory;
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindParam(":idcategory", $idcategory, PDO::PARAM_INT);

            $stmt->execute(); 

            if ($stmt->rowCount()) {
               // $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Product');
                return true; //ENCONTRADO
            } else {
                return false; // NO ENCONTRADO
            }
        } catch (PDOException $e) {
            return NULL; //ERROR
        }
    }

}
