<?php

require_once "model/persist/CategoryFileDAO.class.php";

/**
 * Description of ProductView
 *
 * @author tarda
 */
class ProductView {

    public function __construct() {
        
    }

    public function display($template = NULL, $content = NULL, $str_categories = NULL) {

        include("view/menu/MainMenu.html");

        $categoryFile = new CategoryFileDAO();
        $categories = $categoryFile->listAll();
        $str_categories = $categories;
       
        if (!empty($template)) {
            include($template);
        }

        include("view/form/MessageForm.php");
    }

}
