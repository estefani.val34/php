<div id="content">
    <form method="post" action="">
        <fieldset>
            <legend>Add product</legend>
            <label>Id *:</label>
            <input type="text" placeholder="Id" name="id" value="<?php if (isset($content)) { echo $content->getId(); } ?>" />
            <label>Name *:</label>
            <input type="text" placeholder="Name" name="name" value="<?php if (isset($content)) { echo $content->getName(); } ?>" />
            <label>Price *:</label>
            <input type="text" placeholder="Price" name="price" value="<?php if (isset($content)) { echo $content->getPrice(); } ?>" />
            <label>Description :</label>
            <textarea  name="description" placeholder="Description" rows="10" cols="30" style="width: 236px; height: 40px; margin-left: 17px; border-radius: 4px;"><?php if (isset($content)) { echo $content->getDescription(); } ?></textarea>
            <label>Category :</label>
             <select name="category">
                    <option value="default">------------</option>
                      <?php
                           foreach($str_categories as $key => $value):
                               $fields = explode(";", $value);
                               echo '<option value="'.$fields[0].'">'.$value.'</option>'; 
                           endforeach;
                      ?>
            </select>
                
            <label>* Required fields</label>
            <input type="submit" name="action" value="add" />
            <input type="submit" name="reset" value="reset" onClick="form_reset(this.form.id); return FALSE;" />
        </fieldset>
    </form>
</div>