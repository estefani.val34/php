<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
//END - manual modification

class CategoryController extends Controller
{
    //BEGIN - manual modifications
    public function listAll() {
        try {            
            $arrCat = Category::all(); //devuelve todas las categorias 
                        
            if (isset($arrCat)) {
                $message = count($arrCat) . " categories found";                
            }
            else {
                $message = "No data found";                
            }
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }
        
        return view('category.list', ['arrCategories' => $arrCat, 'message' => $message]);//llama a la vista y le pasamos los parametros 
    }


    public function find(Request $request) {        
        $message ="";
        try {                        
            $objCat = Category::findOrFail($request->id);
            
            //return redirect()->to('/category/edit/' . $request->id);        
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
            return view('category.find', ['message' => $message]); 
        }

        //return view('category.find', ['id' => $request->id, 'message' => $message]);
        return view('category.find', ['id' => $objCat->id, 'name' => $objCat->name, 'message' => $message]);        
    }

    
    public function edit($id) {
        try {                        
            $objCat = Category::findOrFail($id);
            
            $message = "Data found";
            // $message=Session::get('message'); //TODO - amb el login

            return view('category.edit', ['objCategory' => $objCat, 'message' => $message]);        
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }

        //return view('category.find', ['id' => $id, 'message' => $message]);
        return redirect()->to('fallback');
    }





    public function create(Request $request)
    {
        $message ="Data created successful";
        // Validate the request...
        try {      
        $category = new Category;

        $category->name = $request->name;

        $category->description = $request->description;

        $category->save();

        } catch(\Exception $e) {
            $message = "No data create - " . $e->getMessage();
            return view('category.create', ['message' => $message]); 
        }
        return view('category.create',['message' => $message]);     
    }

    //END - manual modifications
}