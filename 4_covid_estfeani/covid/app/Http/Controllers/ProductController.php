<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Product;

class ProductController extends Controller
{
    public function listAll() {
        try {            
            $arrCat = Product::all(); //devuelve todas las categorias 
                        
            if (isset($arrCat)) {
                $message = count($arrCat) . " Products found";                
            }
            else {
                $message = "No data found";                
            }
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }
        
        return view('product.list', ['arrproducts' => $arrCat, 'message' => $message]);//llama a la vista y le pasamos los parametros 
    }

    public function find(Request $request) {        
        $message ="";
        try {                        
            $objCat = Product::findOrFail($request->id);
            
            //return redirect()->to('/product/edit/' . $request->id);        
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
            return view('product.find', ['message' => $message]); 
        }

        //return view('product.find', ['id' => $request->id, 'message' => $message]);
        return view('product.find', ['id' => $objCat->id, 'name' => $objCat->name, 'message' => $message]);        
    }


    public function edit(Request $request) {
        try {                        
            $objCat = Product::findOrFail($request->$id);
            
            //$message = "Data found";
            $message=Session::get('message');

            return view('product.edit', ['objproduct' => $objCat, 'message' => $message]);        
        } catch(\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }

        //return view('product.find', ['id' => $id, 'message' => $message]);
        return redirect()->to('fallback');
    }




    public function create(Request $request)
    {
        $message ="Data created successful";
        // Validate the request...
        try {      
        $product = new Product;

        $product->name = $request->name;

        $product->description = $request->description;

        $product->save();

        } catch(\Exception $e) {
            $message = "No data create - " . $e->getMessage();
            return view('product.create', ['message' => $message]); 
        }
        return view('product.create',['message' => $message]);     
    }

}
