<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    //BEGIN - manual modifications
    protected $fillable = ['name', 'description'];
    //END - manual modifications
    
}
