<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//BEGIN - exemples routes

Route::get('/hello', function ()  
 {      
return "Hello World";  
});
//redirect exemple
Route::redirect('hello2','hello'); 

//parameter
Route::get('/hello/{name}', function($name)  
{  
  return "Hello ".$name;   
}
);

//several parameters
Route::get('/params/{id}/{name}', function($id,$name)  
{  
  return "id number is : ". $id ." ".$name;   
}
);

//optional parameter
Route::get('hello3/{name?}', function ($name = 'Maria') {  
    return "Hola ".$name;  
}); 

//END - exemples routes

//BEGIN - categories

Route::view('/home', 'home');
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/category/list', 'CategoryController@listAll');# llama a el controlador y a ese método listAll
//END - categories

Route::view('/category/find', 'category.find')-> name('catfind');# la vista, el formulario
Route::post('/category/find', 'CategoryController@find');# al boton de buscar 

Route::view('/category/edit/{id}', 'category.edit')-> name('catedit');# la vista, el formulario
Route::post('/category/edit/{id}', 'CategoryController@edit');# al boton de buscar 


Route::view('/category/create', 'category.create')-> name('catcreate');# la vista, el formulario
Route::post('/category/create', 'CategoryController@create');# al boton de buscar 

//product


Route::get('/product/list', 'ProductController@listAll');# al boton de buscar 


Route::view('/product/find', 'product.find')-> name('profind');# la vista, el formulario
Route::post('/product/find', 'ProductController@find');# al boton de buscar 

Route::view('/product/edit', 'product.edit')-> name('proedit');# la vista, el formulario
Route::post('/product/edit', 'ProductController@edit');# al boton de buscar 


Route::view('/product/create', 'product.create')-> name('procreate');# la vista, el formulario
Route::post('/product/create', 'ProductController@create');# al boton de buscar 