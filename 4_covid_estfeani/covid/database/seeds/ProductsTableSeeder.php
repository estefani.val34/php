<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //BEGIN - manual modifications
        DB::table('products')->delete();
        
        //afegir 1 element amb valors fixes
        DB::table('products')->insert([
            'name' => 'Cosmetic',
            'description' => 'Cosmetic products',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        //afegir 1 element amb helper function Str::random
        // see https://laravel.com/docs/7.x/helpers
        DB::table('products')->insert([
            'name' => Str::random(15),
            'description' => Str::random(30),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        //generar 5 elements amb  factory 
        factory(Product::class, 5)->create();
        //END - manual modifications
    }
}
