<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

//genra datos aleatoris para pruebas en la BD. GENERA LOS DATOS 
$factory->define(Category::class, function (Faker $faker) {
    return [
        //BEGIN - manual modifications
        'name' => $faker->unique()->name,
        'description' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        //un exemple de com seria per donar un valor d'una llista:
        //'type' => $faker->randomElement(['Health','Cosmetic']),
        //END - manual modifications
    ];
});
