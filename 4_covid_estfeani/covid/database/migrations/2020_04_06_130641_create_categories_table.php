<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            //BEGIN - manual modifications 
            //change id to increments
            //$table->id();
            $table->increments('id');
            //add name as string 50 lenght, and unique
            $table->string('name', 50)->unique();
            //add description as text 100 lenght, don't allow null
            $table->text('description', 100)->nullable();
            //END - manual modifications
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
