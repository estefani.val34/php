<?php

class ClientFormValidation {

    const ADD_FIELDS = array('username', 'password', 'age', 'role', 'active');
    const MODIFY_FIELDS = array('username', 'password', 'age', 'role', 'active');
    const DELETE_FIELDS = array('username');
    const SEARCH_FIELDS = array('username');
    const NUMERIC = "/[^0-9]/";
    const ALPHABETIC = "/[^a-z A-Z]/";
   

    public static function checkData($fields) {
        $username = NULL;
        $age = NULL;
        $role = NULL;
        $active = NULL;
        $password = NULL;

        foreach ($fields as $field) {
            switch ($field) {
                case 'username':
                    $username = trim(filter_input(INPUT_POST, 'username'));
                    $usernameValid = !preg_match(self::ALPHABETIC, $username);
                    if (empty($username)) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['empty_username']);
                    } else if ($usernameValid == FALSE) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_username']);
                    }
                    break;
                case 'age':
                    $age = trim(filter_input(INPUT_POST, 'age'));
                    $ageValid = !preg_match(self::NUMERIC, $age);
                    if ($ageValid == FALSE) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_age']);
                    }
                    break;
                case 'role':
                    $role = trim(filter_input(INPUT_POST, 'role'));
                    
                    break;
                case 'active':
                    $active = trim(filter_input(INPUT_POST, 'active'));
                   // var_dump($active);
                   if ($active!="0" && $active!="1") {
                       array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_active']);
                   }
                    break;
                case 'password':
                    $password = trim(filter_input(INPUT_POST, 'password'));
                    $passwordValid = !preg_match(self::ALPHABETIC, $password);
                    if (empty($password)) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['empty_password']);
                    } else if ($passwordValid == FALSE) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_password']);
                    }
                    break;
            }
        }

        $client = new Client($username, $password, $age, $role, $active);

        return $client;
    }

}
