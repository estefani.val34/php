<div id="content">
    <fieldset>
        <legend>User list</legend>    
        <?php
            if (isset($content)) {
                echo <<<EOT
                    <table>
                        <tr>
                            <th>Username</th>
                            <th>Age</th>
                            <th>Role</th>
                            <th>Active</th>
                            
                        </tr>
EOT;
                foreach ($content as $client) {
                    echo <<<EOT
                        <tr>
                            <td>{$client->getUsername()}</td>
                            <td>{$client->getAge()}</td>
                            <td>{$client->getRole()}</td>
                            <td>{$client->getActive()}</td>
                        </tr>
EOT;
                }
                echo <<<EOT
                    </table>
EOT;
            }
        ?>
    </fieldset>
</div>
