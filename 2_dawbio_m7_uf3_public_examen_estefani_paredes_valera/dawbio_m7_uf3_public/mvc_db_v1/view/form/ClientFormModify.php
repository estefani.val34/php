<div id="content">
    <form method="post" action="">
        <fieldset>
            <legend>Modify client</legend>
            <label>Username *:</label>
            <input type="text" placeholder="Username" name="username" value="<?php if (isset($content)) { echo $content->getUsername(); } ?>" />
            <label>Password *:</label>
            <input type="text" placeholder="Password" name="password" value="<?php if (isset($content)) { echo $content->getPassword(); } ?>" />
            <label>Age :</label>
            <input type="text" placeholder="Age" name="age" value="<?php if (isset($content)) { echo $content->getAge(); } ?>" />
            <label>Role :</label>
            <select name="role">
                      <?php  
                     
                                 if (isset($content)  ) {
                                     if($content->getRole()  == "Basic"){
                                          echo '<option value="'.$roles["Basic"].'" selected >'.$roles["Basic"].'</option>';
                                          echo '<option value="'.$roles["Advanced"].'" >'.$roles["Advanced"].'</option>';
                                     }else{
                                         echo '<option value="'.$roles["Advanced"].'" selected >'.$roles["Advanced"].'</option>';
                                          echo '<option value="'.$roles["Basic"].'"  >'.$roles["Basic"].'</option>';
                                     }
                                   }else{
                                        echo '<option value="'.$roles["Basic"].'" selected >'.$roles["Basic"].'</option>';
                                        echo '<option value="'.$roles["Advanced"].'">'.$roles["Advanced"].'</option>'; 
                                   }
                      ?>
                  
            </select>
            
            <label>Active :</label>
                    <?php  
                           
                                 if (isset($content) ) {
                                    if( $content->getActive()== "1"){
                                          echo ' <input type="radio" name="active" value="1" checked />'."YES";
                                          echo ' <input type="radio" name="active" value="0"/>'."NO";
                                    }else{
                                        echo ' <input type="radio" name="active" value="1"/>'."YES";
                                        echo ' <input type="radio" name="active" value="0" checked />'."NO";
                                    }

                                 }else{
                                     echo ' <input type="radio" name="active" value="1" />'."YES";
                                     echo ' <input type="radio" name="active" value="0" />'."NO";
                                 }
                            
                    ?>
       
            <label>* Required fields</label>
            <input type="submit" name="action" value="modify" />
            <input type="submit" name="action" value="delete" />
            <input type="submit" name="reset" value="reset" onClick="form_reset(this.form.id); return FALSE;" />
        </fieldset>
    </form>
</div>