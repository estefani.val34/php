<?php

// File
//require_once "model/persist/CategoryFileDAO.class.php";
/*
 * bindParam(:id,$var,); trabaj por referencia  --> crear una variable con $var=$category->getId
 * */

// Database
require_once "model/persist/CategoryDbDAO.class.php";

class CategoryModel {

    private $dataCategory;

    public function __construct() {
        // File
        //$this->dataCategory=CategoryFileDAO::getInstance();
        // Database
        $this->dataCategory = CategoryDbDAO::getInstance();
    }

    /**
     * insert a category
     * @param $category Category object to insert
     * @return TRUE or FALSE
     */
    public function add($category): bool {
        $result = $this->dataCategory->add($category);

        if ($result == FALSE && empty($_SESSION['error'])) {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['insert'];
        }

        return $result;
    }

    /**
     * update a category
     * @param $category Category object to update
     * @return TRUE or FALSE
     */
    public function modify($category): bool {
        $result = $this->dataCategory->modify($category);
        if ($result == FALSE) {//false
            $_SESSION['error'] = ProductMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a category
     * @param $id string Category Id to delete
     * @return TRUE or FALSE

      public function delete($id): bool {
      $result = $this->dataCategory->delete($id);
      if (!$result) {
      $_SESSION['error'] = ProductMessage::ERR_DAO['delete'];
      }
      return $result;
      } */
    public function delete($id): bool {
        $result = FALSE;
        $productModel = new ProductModel();
        $categoryUsed = $productModel->categoryInProduct($id); //bool, is exists products of this category
        
        if (is_null($categoryUsed)) {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['error'];
            return false;
        }
        
        if (!$categoryUsed) {
            $result = $this->dataCategory->delete($id);
            if (!$result) {
                $_SESSION['error'] = CategoryMessage::ERR_DAO['delete'];
            }
        } else {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['used'];if (is_null($categoryUsed)) {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['error'];
        }
        }
        return $result;
    }

    /**
     * select all categories
     * @param void
     * @return array of Category objects or array void
     */
    public function listAll(): array {
        $categories = $this->dataCategory->listAll();

        return $categories;
    }

    /**
     * select a category by Id
     * @param $id string Category Id
     * @return Category object or NULL
     */
    public function searchById($id) {
        $result = $this->dataCategory->searchById($id);

        return $result;
    }

}
