<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectDb.class.php";

class ClientDbDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    public function __construct() {
        $this->connect = (new ConnectDb())->getConnection();
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): ClientDbDAO {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($client): bool {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {
            $sql = <<<SQL
                INSERT INTO user (username,password,age,role,active) VALUES (:username,:password,:age,:role,:active);
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":username", $client->getUsername() , PDO::PARAM_STR);
             $stmt->bindValue(":password", $client->getPassword(), PDO::PARAM_STR);
            $stmt->bindValue(":age", $client->getAge(), PDO::PARAM_INT);
            $stmt->bindValue(":role", $client->getRole(), PDO::PARAM_STR);
            $stmt->bindValue(":active", $client->getActive(), PDO::PARAM_INT);
           
            $stmt->execute(); // devuelve TRUE o FALSE

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function modify($client): bool {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {

            $sql = <<<SQL
                    UPDATE user SET username=:username,password=:password,age=:age,role=:role,active=:active
                        WHERE username=:username;
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":username", $client->getUsername(), PDO::PARAM_STR);
            $stmt->bindValue(":password", $client->getPassword(), PDO::PARAM_STR);
            $stmt->bindValue(":age", $client->getAge(), PDO::PARAM_INT);
            $stmt->bindValue(":role", $client->getRole(), PDO::PARAM_STR);
            $stmt->bindValue(":active", $client->getActive(), PDO::PARAM_INT);
            

            $stmt->execute();

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function delete($username): bool {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return FALSE;
        };

        try {

            $sql = <<<SQL
                    DELETE FROM user WHERE username=:username;
SQL;
            $stmt = $this->connect->prepare($sql);
            $stmt->bindValue(":username", $username, PDO::PARAM_STR);

            $stmt->execute();

            if ($stmt->rowCount()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public function listAll(): array {
        $result = array();

        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return $result;
        };

        try {
            $sql = <<<SQL
               
                     SELECT username,password,age,role,active FROM user;
SQL;

            $result = $this->connect->query($sql); // devuelve los datos

            $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'User');

            return $result->fetchAll();
        } catch (PDOException $e) {
            return $result;
        }

        return $result;
    }

    public function searchById($username) {
        if ($this->connect == NULL) {
            $_SESSION['error'] = "Unable to connect to database";
            return NULL;
        };

        try {
            $sql = <<<SQL
                SELECT  username,password,age,role,active FROM user WHERE username=:username;
SQL;

            $stmt = $this->connect->prepare($sql);
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);

            $stmt->execute(); // devuelve TRUE o FALSE
           // print_r($stmt->execute());
            if ($stmt->rowCount()) {
                $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'User');
                return $stmt->fetch();
            } else {
                return NULL;
            }
        } catch (PDOException $e) {
            return NULL;
        }
    }

}
