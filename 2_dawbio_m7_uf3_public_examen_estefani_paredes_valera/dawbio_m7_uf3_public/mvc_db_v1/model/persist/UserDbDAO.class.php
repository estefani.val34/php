<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectDb.class.php";

class UserDbDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    public function __construct() {
        $this->connect = (new ConnectDb())->getConnection();
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): UserDbDAO {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    
    public function searchByUsername($username) {
        try{
            
            $sql=<<<SQL
                SELECT username, password, age, role, active
                            FROM user WHERE username=:username;
SQL;
 
                    $stmt=$this->connect->prepare($sql);
                    $stmt->bindParam(":username", $username, PDO::PARAM_STR);
                    $stmt->execute(); // devuelve true o false

                    if ($stmt->rowCount()) {
                        
                        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'User');
                        
                        $usuari= $stmt->fetch();
                        print_r($usuari);
                        return $usuari;
                    }
                    return NULL;
            
        } catch (PDOException $ex) {
                    return NULL;            
        }
  
    }
    
    public function add($user): bool {

    }

    public function modify($user): bool {
        /* TODO */
    }

    public function delete($id): bool {
        /* TODO */
    }

    public function listAll(): array {

    }

    public function searchById($id) {
  
    }

}
